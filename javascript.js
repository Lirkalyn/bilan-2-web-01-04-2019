var chiffre1,
	chiffre2,
	chiffre3;

var veridom = function() {
	chiffre1 = parseInt(document.forms["formulaire"].Chiffre_1.value);
	chiffre2 = parseInt(document.forms["formulaire"].Chiffre_2.value);
	if (isNaN(chiffre1)){
		alert("vous avez oublier de mettre un nombre dans chiffre 1 ou vous avez mis du texte");
	}
	else if (isNaN(chiffre2)){
		alert("vous avez oublier de mettre un nombre dans chiffre 2 ou vous avez mis du texte");
	}
	else if (chiffre1 >= chiffre2){
		alert("chiffre 2 doit être strictement supérieur a chiffre 1");
	}
	else {
		chiffre3 = Math.floor(Math.random() * (chiffre2 - chiffre1 + 1) + chiffre1);
		document.forms["formulaire"].Chiffre_3.value = chiffre3;
	}
}